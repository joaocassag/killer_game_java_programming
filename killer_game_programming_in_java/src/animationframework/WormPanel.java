
package animationframework;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import javax.swing.JPanel;

public class WormPanel extends JPanel {
    private static final int PWIDTH = 500;
    private static final int PHEIGHT = 400;
    WormChase wcTop;
    Worm fred;
    
    private Thread animator;
    private volatile boolean running = false;
    
    private volatile boolean gameOver = false;
    
    private Graphics dbg;
    private Image dbImage = null;
    //more variables
    private long period;
    
    private static final int NO_DELAYS_PER_YIELD=16;
    private int MAX_FRAME_SKIPS=5;
    private boolean isPaused;
    
    Font font;
    private final FontMetrics metrics;
    private int NUM_FPS=10;
    
    double averageUPS=0.0;
    private int timeSpentInGame=0;
    private Object obs;
    private long gameStartTime;
    long framesSkipped=0L;
    long totalFramesSkipped=0L;
    
    private long statsInterval=0L;
    private long prevStatsTime;
    long totalElapsed=0;
    private long MAX_STATS_INTERVAL=1000L;
    int totalElapsedTime=0;
    
    private int frameCount;
    private final double[] fpsStore;
    private final double[] upsStore;
    private int statsCount=0;
    private double averageFPS=0.0;
    
    private DecimalFormat df = new DecimalFormat("0.##");//2dp
    private DecimalFormat timedf = new DecimalFormat("0.####");// 4dp
    
    public WormPanel(WormChase wc, long period){
        wcTop= wc;
        this.period = period;
        
        setBackground(Color.white);
        setPreferredSize(new Dimension(PWIDTH,PHEIGHT));
        
        setFocusable(true);
        requestFocus();
        readyForTermination();
        
        //create game components
        obs = new Obstacles(wcTop);
        fred = new Worm(PWIDTH,PHEIGHT,obs);
        
        addMouseListener(new MouseAdapter(){
            public void mousePressed(MouseEvent e){
                testPress(e.getX(),e.getY());
            }
        });
        
        //set up message font
        font = new Font("SansSerif",Font.BOLD,24);
        metrics = this.getFontMetrics(font);
        
        //intialise timing elements
        fpsStore = new double[NUM_FPS];
        upsStore = new double[NUM_FPS];
        for(int i =0;i<NUM_FPS;i++){
            fpsStore[i]=0.0;
            upsStore[i]=0.0;
        }
    }
    
    private void testPress(int x, int y) {
        if(!isPaused && !gameOver){
            if(fred.nearHead(x,y)){
                gameOver =true;
                score = (40 - timeSpentInGame)+40-obs.getNumObstacles();
            }else{
                if(!fred.touchedAt(x,y))
                    obs.add(x,y);
            }
        }
    }
    
    private void readyForTermination() {
        addKeyListener(new KeyAdapter(){
            //listen for esc, q, end, crtl-c
            public void keyPressed(KeyEvent e){
                int keyCode = e.getKeyCode();
                if(keyCode == KeyEvent.VK_ESCAPE ||(keyCode == KeyEvent.VK_Q) ||keyCode == KeyEvent.VK_END ||
                   (keyCode == KeyEvent.VK_C) && e.isControlDown()){
                    running = false;
                }
            }
        });
    }
    
    public void pauseGame(){
        isPaused = true;
    }
    
    public void resumeGame(){
        isPaused = false;
    }
    
    public void stopGame(){
        running = false;
    }
    
    public void run() {
        long beforeTime, afterTime, timeDiff, sleepTime;
        long overSleepTime=0L;
        int noDelays=0;
        long excess = 0L;
        gameStartTime = System.currentTimeMillis();
        prevStatsTime = gameStartTime;
        //beforeTime = J3DTimer.getValue();
        beforeTime = gameStartTime;
        
        running = true;
        while(running){
            gameUpdate();
            gameUpdate();//updated again
            
            gameRender();
            paintScreen();
            
            //afterTime = J3DTimer.getValue();
            afterTime = System.currentTimeMillis();
            timeDiff = afterTime - beforeTime;
            sleepTime = (period - timeDiff) - overSleepTime;
            
            if(sleepTime>0){
                try{Thread.sleep(sleepTime/1_000_000L);}catch(Exception e){}
                //overSleepTime = (J3DTimer.getValue() - afterTime) - sleepTime;
                overSleepTime = (System.currentTimeMillis() - afterTime) - sleepTime;
            }else{
                excess -= sleepTime;
                overSleepTime=0L;
                if(++noDelays>= NO_DELAYS_PER_YIELD){
                    Thread.yield();
                    noDelays=0;
                }
            }
            //beforeTime = J3DTimer.getValue();
            beforeTime = System.currentTimeMillis();
            
            //if frame animation is taking too long, update the gae state wihtout rendering it,
            //to get the updates/sec nearer to the required FPS
            int skips =0;
            while((excess>period) && (skips < MAX_FRAME_SKIPS)){
                excess -=period;
                gameUpdate();//update state but don't render
                skips++;
            }
            framesSkipped+=skips;
            storeStats();
        }
        printStats();
        System.exit(0);
    }
    
    private void gameUpdate() {
        if(!isPaused && !gameOver)
            ;//update game state ...
    }

    private void gameRender() {
        if(dbImage ==null){
            dbImage = createImage(PWIDTH,PHEIGHT);
            if(dbImage ==null){
                System.out.println("dbImage is null");
                return;
            }
            else dbg = dbImage.getGraphics();
        }
        
        //clear the backgrouund
        dbg.setColor(Color.white);
        dbg.fillRect(0,0, PWIDTH,PHEIGHT);
        
        //report average FPS and UPS at top left
        dbg.drawString("Average FSP/UPS: "+df.format(averageFPS)+", "+df.format(averageUPS),20,25);
        dbg.setColor(Color.black);
        
        //draw game elements
        obs.draw(dbg);
        fred.draw(dbg);
        
        if(gameOver)
            gameOverMessage(dbg);
    }

    private void gameOverMessage(Graphics g) {
        //code to calculate x and y ...
        String msg = "";
        int x=0, y=0;
        g.drawString(msg, x,y);
    }
    
    private void paintScreen() {
        Graphics g;
        try{
            g = this.getGraphics();
            if(g!=null && dbImage!=null)
                g.drawImage(dbImage,0,0,null);
            Toolkit.getDefaultToolkit().sync();
            g.dispose();
        }catch(Exception e){System.out.println("Graphics context error: "+e);}
    }

    private void storeStats() {
        frameCount++;
        statsInterval+=period;
        
        if(statsInterval >=MAX_STATS_INTERVAL){
            long timeNow = System.currentTimeMillis();
            timeSpentInGame = (int)((timeNow - gameStartTime)/1_000_000_000);
            wcTop.setTimeSpent(timeSpentInGame);
            
            long realElapsed = timeNow - prevStatsTime;
            totalElapsedTime +=realElapsed;
            
            double timingError = (double)((realElapsed-statsInterval)/statsInterval)*100.0;
            totalFramesSkipped+=framesSkipped;
            
            double actualFPS =0;
            double actualUPS=0;
            if(totalElapsed>0){
                actualFPS = ((double) (frameCount+totalFramesSkipped)/totalElapsed*1_000_000_000L);
            }
            fpsStore[(int)statsCount%NUM_FPS] = actualFPS;
            upsStore[(int)statsCount%NUM_FPS] = actualFPS;
            
            double totalFPS=0.0;
            double totalUPS=0.0;
            for(int i =0;i<NUM_FPS;i++){
                totalFPS+=fpsStore[i];
                totalUPS+=upsStore[i];
            }
            
            if(statsCount < NUM_FPS){
                averageFPS = totalFPS/NUM_FPS;
                averageUPS = totalUPS/NUM_FPS;
            }
            framesSkipped=0;
            prevStatsTime = timeNow;
            statsInterval = 0L;//reset
        }
    }

    private void printStats() {
        System.out.println("Frame Count/Loss: "+frameCount+"/"+totalFramesSkipped);
        System.out.println("Average FPS: "+ df.format(averageFPS));
        System.out.println("Average UPS: "+ df.format(averageUPS));
        System.out.println("Time Spent: "+timeSpentInGame+" secs");
        System.out.println("Boxes used: "+obs.getNumObstacles());
    }
    
}
