
package animationframework;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent ;
import javax.swing.JPanel;

//import com.sun.j3d.utils.timer.J3DTimer;

public class GamePanel extends JPanel implements Runnable {
    private static final int PWIDTH = 500;
    private static final int PHEIGHT = 400;
    
    private Thread animator;
    private volatile boolean running = false;
    
    private volatile boolean gameOver = false;
    
    private Graphics dbg;
    private Image dbImage = null;
    //more variables
    private long period=10;
    
    private static final int NO_DELAYS_PER_YIELD=16;
    private int MAX_FRAME_SKIPS=5;
    private boolean isPaused;
    
    
    public GamePanel(){
        //TODO: calculate period
        setBackground(Color.white);
        setPreferredSize(new Dimension(PWIDTH,PHEIGHT));
        
        setFocusable(true);
        requestFocus();
        readyForTermination();
        
        //create game components
        
        //listen for mouse presses
        addMouseListener(new MouseAdapter(){
            public void mousePressed(MouseEvent e){
                testPress(e.getX(),e.getY());
            }

            /*private void testPress(int x, int y) {
                if(!gameOver){
                    //do something
                }
            }*/
        });
    }
    
    public void addNotify(){
        //wait for the JPanel to be added to the JFrame/JApple before starting
        super.addNotify();
        startGame();
    }

    private void startGame() {
        if(animator==null ||running){
            animator = new Thread(this);
            animator.start();
        }
    }
    
    public void stopGame(){
        running = false;
    }
    
    @Override
    public void run() {
        long beforeTime, afterTime, timeDiff, sleepTime;
        long overSleepTime=0L;
        int noDelays=0;
        long excess = 0L;
        
        //beforeTime = J3DTimer.getValue();
        beforeTime = System.currentTimeMillis();
        
        running = true;
        while(running){
            gameUpdate();
            gameUpdate();//updated again
            
            gameRender();
            paintScreen();
            
            //afterTime = J3DTimer.getValue();
            afterTime = System.currentTimeMillis();
            timeDiff = afterTime - beforeTime;
            sleepTime = (period - timeDiff) - overSleepTime;
            
            if(sleepTime>0){
                try{Thread.sleep(sleepTime/1_000_000L);}catch(Exception e){}
                //overSleepTime = (J3DTimer.getValue() - afterTime) - sleepTime;
                overSleepTime = (System.currentTimeMillis() - afterTime) - sleepTime;
            }else{
                excess -= sleepTime;
                overSleepTime=0L;
                if(++noDelays>= NO_DELAYS_PER_YIELD){
                    Thread.yield();
                    noDelays=0;
                }
            }
            //beforeTime = J3DTimer.getValue();
            beforeTime = System.currentTimeMillis();
            
            //if frame animation is taking too long, update the gae state wihtout rendering it,
            //to get the updates/sec nearer to the required FPS
            int skips =0;
            while((excess>period) && (skips < MAX_FRAME_SKIPS)){
                excess -=period;
                gameUpdate();//update state but don't render
                skips++;
            }
        }
        System.exit(0);
    }

    private void gameUpdate() {
        if(!isPaused && !gameOver)
            ;//update game state ...
    }

    private void gameRender() {
        if(dbImage ==null){
            dbImage = createImage(PWIDTH,PHEIGHT);
            if(dbImage ==null){
                System.out.println("dbImage is null");
                return;
            }
            else dbg = dbImage.getGraphics();
        }
        
        //clear the backgrouund
        dbg.setColor(Color.white);
        dbg.fillRect(0,0, PWIDTH,PHEIGHT);
        
        //draw game elements
        
        if(gameOver)
            gameOverMessage(dbg);
    }

    private void gameOverMessage(Graphics g) {
        //code to calculate x and y ...
        String msg = "";
        int x=0, y=0;
        g.drawString(msg, x,y);
    }

    private void readyForTermination() {
        addKeyListener(new KeyAdapter(){
            //listen for esc, q, end, crtl-c
            public void keyPressed(KeyEvent e){
                int keyCode = e.getKeyCode();
                if(keyCode == KeyEvent.VK_ESCAPE ||(keyCode == KeyEvent.VK_Q) ||keyCode == KeyEvent.VK_END ||
                   (keyCode == KeyEvent.VK_C) && e.isControlDown()){
                    running = false;
                }
            }
        });
    }
    
    private void testPress(int x, int y) {
        if(!isPaused && !gameOver){
            //do something
        }
    }

    private void paintScreen() {
        Graphics g;
        try{
            g = this.getGraphics();
            if(g!=null && dbImage!=null)
                g.drawImage(dbImage,0,0,null);
            Toolkit.getDefaultToolkit().sync();
            g.dispose();
        }catch(Exception e){System.out.println("Graphics context error: "+e);}
    }
    
    public void pauseGame(){
        isPaused = true;
    }
    
    public void resumeGame(){
        isPaused = false;
    }
    
}
