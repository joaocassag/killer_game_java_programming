
package animationframework;

import java.awt.event.WindowEvent;
import javax.swing.JTextField;

public class WormChase {
    static int DEFAULTS_FPS=80;
    WormPanel wp;
    JTextField jtfBox;
    JTextField jtfTime;
    
     private long period=10;
    
    public static void main(String args[]){
        int fps = DEFAULTS_FPS;
        if(args.length!=0)
            fps = Integer.parseInt(args[0]);
        
        long period = (long) 1000.0/fps;
        System.out.println("fps: "+fps+"; period: "+period+" ms");
        
        new WormChase(period*1_000_000L);
    }

    private WormChase(long l) {
        period = l;
    }
    
    public void setBoxNumber(int no){
        jtfBox.setText("Boxes used: "+no);
    }
    
    public void setTimeSpent(long t){
        jtfTime.setText("Time Spent: "+t+" ms");
    }
    
    public void windowActivated(WindowEvent e){
        wp.resumeGame();
    }
    
    public void windowDeActivated(WindowEvent e){
        wp.pauseGame();
    }
    
    public void windowDeiconified(WindowEvent e){
        wp.resumeGame();
    }
    
    public void windowIconified(WindowEvent e){
        wp.pauseGame();
    }
    
    public void windowClosing(WindowEvent e){
        wp.stopGame();
    }
    
}
